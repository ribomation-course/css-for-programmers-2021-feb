const { src, dest, series, parallel } = require('gulp');
const minHTML = require('gulp-htmlmin');
const minCSS  = require('gulp-clean-css');
const del     = require('delete');

const srcDir = './src'
const bldDir = './build'

function clean(next) {
    del([bldDir + '/*'], next);
}

function css() {
    return src(srcDir + '/*.css')
        .pipe(minCSS({
            level: 2
        }))
        .pipe(dest(bldDir))
        ;
}

function html() {
    return src(srcDir + '/*.html')
        .pipe(minHTML({
            collapseWhitespace: true,
            html5: true,
            removeComments: true,
            maxLineLength: 60,
        }))
        .pipe(dest(bldDir))
        ;
}

exports.default = series(clean, parallel(css, html));

