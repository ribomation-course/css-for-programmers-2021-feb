
function toast(title, message, style = 'success') {
    const pane = document.querySelector('.toast');
    const titlePane = pane.querySelector('h3')
    const messagePane = pane.querySelector('p')

    titlePane.innerText = title
    messagePane.innerText = message

    pane.classList.add('toast--' + style)
    pane.classList.add('toast--show')
    setTimeout(() => {
        pane.classList.remove('toast--show')
        pane.classList.remove('toast--' + style)
    }, 4000)
}



document.querySelector('.showSuccess')
    .addEventListener('click', (ev) => {
        ev.preventDefault();
        toast('Congrats...', 'You just won the Tjolla-Hopp lottery!', 'success')
    });

document.querySelector('.showFailure')
    .addEventListener('click', (ev) => {
        ev.preventDefault();
        toast('Sorry!', 'The last operation went down in flames', 'failure')
    });
