async function invokeServer(url) {
    return new Promise((success, failure) => {
        const http = new XMLHttpRequest();
        http.onreadystatechange = () => {
            if (http.readyState === XMLHttpRequest.DONE) {
                if (http.status === 200) {
                    success(JSON.parse(http.responseText))
                } else {
                    failure(http.status)
                }
            }
        }
        http.open('GET', url, true);
        http.send();
    })
}

async function invokeServerModern(url) {
    const response = await fetch(url)
    return await response.json()
}

(async () => {
    try {
        const loremUrl   = 'https://baconipsum.com/api/?type=meat-and-filler&paras=3'
        const loremItems = await invokeServer(loremUrl)
        const ul         = document.createElement('ul');
        loremItems.forEach(p => {
            const li = document.createElement('li');
            li.appendChild(document.createTextNode(p));
            ul.appendChild(li);
        });
        document.querySelector('#lorem').appendChild(ul)
    } catch (err) {
        console.error('failed: %o', err);
    }

    try {
        const userUrl  = 'https://randomuser.me/api/'
        const userData = await invokeServerModern(userUrl)
        const [user]   = userData.results
        const name     = `${user.name.first} ${user.name.last}`
        const city     = user.location.city
        const email    = user.email
        const photo    = user.picture.large
        const userPane = document.querySelector('#user')
        userPane.querySelector('img').src      = photo
        userPane.querySelector('h2').innerText = name
        userPane.querySelector('h4').innerText = email
        userPane.querySelector('h5').innerText = city
    } catch (err) {
        console.error('failed: %o', err);
    }
})()
