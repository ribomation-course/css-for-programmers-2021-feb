(function()  {
    'use strict';
    const OPEN = 'is-open';
    const toggles = document.querySelectorAll('.dropdown > button');
    toggles.forEach(t => {
        t.addEventListener('click', ev => {
            ev.preventDefault();
            const dropdown = ev.target.parentNode;
            dropdown.classList.toggle(OPEN);

            if (dropdown.classList.contains(OPEN)) {
                const pane = dropdown.querySelector('div');
                pane.addEventListener('mouseleave', ev => {
                    dropdown.classList.remove(OPEN);
                });
            }else {
                pane.removeEventListener('mouseleave');
            }
        });
    });
}());

