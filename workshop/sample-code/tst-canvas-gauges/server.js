const express = require('express')
const cors = require('cors')
const app = express()

app.use(cors())
app.use(express.static('.'))

const genValue = (min, max) => min + Math.floor((max - min) * Math.random())

app.get('/speed/:max/:min', (req, res) => {
    const min = parseInt(req.params.min) || 0
    const max = parseInt(req.params.max) || 200
    res.json({
        value: genValue(min, max)
    })
})

app.get('/temperature/:max/:min', (req, res) => {
    const min = parseInt(req.params.min) || -50
    const max = parseInt(req.params.max) || +150
    res.json({
        value: genValue(min, max)
    })
})

app.listen(3000, function () {
    console.log('server running: http://localhost:3000/')
})
