const devBaseUrl = 'http://localhost:5000/api/tools/'
const prodBaseUrl = '/api/tools/'

export function backendUrl(name) {
    if (process.env.NODE_ENV === 'production') {
        return prodBaseUrl + name
    } else {
        return devBaseUrl + name
    }
}

