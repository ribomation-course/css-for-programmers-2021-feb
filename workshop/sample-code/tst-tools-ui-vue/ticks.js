function* generate(first, step, last) {
    let value = first
    while (value <= last) {
        yield value
        value += step
    }
    return value
}


export function createTicks(first, step, last) {
    return [...generate(first, step, last)]
}

