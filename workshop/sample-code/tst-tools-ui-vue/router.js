import Vue from 'vue'
import VueRouter from 'vue-router'

import HomePage from '../pages/home'
import SpeedPage from '../pages/speed'
import TemperaturePage from '../pages/temperature'
import NotFoundPage from '../pages/not-found'

Vue.use(VueRouter)

const routes = [
    {path: '/home', name: 'home', component: HomePage},
    {path: '/speed', name: 'speed', component: SpeedPage},
    {path: '/temperature', name: 'temperature', component: TemperaturePage},
    {path: '/', redirect: {name: 'home'}},
    {path: '*', component: NotFoundPage}
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
