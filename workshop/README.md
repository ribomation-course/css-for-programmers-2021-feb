# Workshop Day

A workshop day targeting 
[C++](https://en.cppreference.com/w/cpp) on the server-side 
and (simple) [Vue.js](https://vuejs.org/) on the client-side.

## Objective
Implement a small tool dashboard in Vue.js, using a C++
  JSON@REST microserver

## Pre-requisites
In order to participate you need to know how to program
in C++ and have at least a brief knowledge of HTML/JS/CSS.


# Installation Instructions
## Server-Side
Although, you can you use any compiler, IDE and OS to implement
the micro-server, as long as it supports C++17. We highly
recommend to use Linux, GCC/G++ and CLion IDE.

Read our generic installation instructions for how 
to set up Linux and C++

* [Linux and C++ Installation](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/linux-and-cxx.md)


## Client-Side
Same set up as for the CSS course

* [NodeJS / NPM / NPX](https://nodejs.org/en/download/)
* [JetBrains WebStorm](https://www.jetbrains.com/webstorm/download)
*  [Google Chrome](https://www.google.com/chrome/browser/desktop/index.html)


